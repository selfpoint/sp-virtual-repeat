# sp-virtual-repeat

An angularjs package for a virtual repeat of elements.

### `sp-virtual-repeat` Directive

##### Bindings:
* `sp-virtual-repeat` Required. The array of which to repeat on.
* `sp-virtual-repeat-item-name` Required. The name of each item in the repeated elements.

##### Usage
#
```
<div sp-virtual-repeat="itemsArray" sp-virtual-repeat-item-name="item" style="max-height: 300px;">
    <div class="item-element" style="height: 20px;">{{item}}</div>
</div>
```

The `sp-virtual-repeat` element (the wrapper) must have either height or max-height for it to not grow with its children.
Each item in the repeat must have height for the repeater to calculate how many items can fit in the wrapper.